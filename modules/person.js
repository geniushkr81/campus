import * as THREE from '../build/three.module.js';

import { RoomEnvironment } from '../jsm/environments/RoomEnvironment.js';
import { OrbitControls } from '../jsm/controls/OrbitControls.js';
import { GLTFLoader } from '../jsm/loaders/GLTFLoader.js';

import { KTX2Loader } from '../jsm/loaders/KTX2Loader.js';
import { MeshoptDecoder } from '../jsm/libs/meshopt_decoder.module.js';
import { PointerLockControls } from '../jsm/controls/PointerLockControls.js';


    // create a scene, that will hold all our elements such as objects, cameras nad lights.
    var scene = new THREE.Scene();
    const sceneMeshes = new THREE.Object3D();
    const raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();
    
  
    // create a camera, wich defines where we`re looking at.
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    // create a render and set the size
    var webGLRenderer = new THREE.WebGLRenderer({antialias: true});
  //  scene.background = new THREE.Texture("../images/sky.jpeg");
      // Load the background texture
     //Load background texture
const loaderTexture = new THREE.TextureLoader();
loaderTexture.load('./images/sky.gif' , function(texture)
            {
             scene.background = texture;  
            
            });

    // Color(0x000000);
    webGLRenderer.setSize(window.innerWidth, window.innerHeight);

    
    // position and point the camera to the center of the scene
   // camera.position.x = 5;
    camera.position.y = 25;
    camera.position.x = -150;
    camera.position.z = 200;
    document.body.appendChild(webGLRenderer.domElement);
    
    var ambientLight = new THREE.AmbientLight(0xb3963e);
    scene.add(ambientLight);
    // add spotlight for the shadows
    var spotLight = new THREE.SpotLight(0xebffff);
    spotLight.position.set(100,140,130);
    spotLight.intensity = 1;
    scene.add(spotLight);
    var spotLight_0 = new THREE.SpotLight(0xff9563);
    spotLight_0.position.set(150,140,130);
    spotLight_0.intensity = 1;
    scene.add(spotLight_0);
    // add the output of the renerer to the html element
    let grid = new THREE.GridHelper(100, 20, 0x0a0a0a, 0x0a0a0a);
    grid.position.set(0, -0.5, 0);
    scene.add(grid);
/*
    let bGeo = new THREE.BoxGeometry(1,1,1);
    let bMat = new THREE.MeshStandardMaterial({color:0x00ff00, wireframe:false});
    let cube = new THREE.Mesh(bGeo, bMat);
    scene.add(cube);

*/
    const ktx2Loader = new KTX2Loader()
    .setTranscoderPath('../js/libs/basis/')
    .detectSupport(webGLRenderer);

    const loader = new GLTFLoader();
    loader.setKTX2Loader(ktx2Loader);
    loader.setMeshoptDecoder(MeshoptDecoder);
    loader.load('../models/campus_01.glb', function(gltf) {
        gltf.scene.position.y = 0;
       scene.add(gltf.scene);
        drawScene();
    });

    const loader_m = new GLTFLoader();
    loader_m.setKTX2Loader(ktx2Loader);
    loader_m.setMeshoptDecoder(MeshoptDecoder);
    loader_m.load('../models/mujeres.glb', function(gltf) {
        gltf.scene.position.x = -330;
        gltf.scene.position.z = 70;
        gltf.scene.position.y = 5;
       scene.add(gltf.scene);
        drawScene();
    });


    const loader_c = new GLTFLoader();
    loader_c.setKTX2Loader(ktx2Loader);
    loader_c.setMeshoptDecoder(MeshoptDecoder);
    loader_c.load('../models/convenciones.glb', function(gltf){
        gltf.scene.scale.set(4,4,4); 
        gltf.scene.position.y = -8;
        gltf.scene.position.x = 205;
        gltf.scene.position.z = -85;
        scene.add(gltf.scene);
        drawScene();
    });
   
    const loader_b = new GLTFLoader();
    loader_b.setKTX2Loader(ktx2Loader);
    loader_b.setMeshoptDecoder(MeshoptDecoder);
    loader_b.load('../models/build01.glb', function(gltf){
     
        gltf.scene.scale.set(0.4, 0.4, 0.4); 
        gltf.scene.position.y = 0;
        gltf.scene.position.x = -50;
        gltf.scene.position.z = 160;
        sceneMeshes.add(gltf.scene);
     
        scene.add(sceneMeshes);
       
        drawScene();
        webGLRenderer.domElement.addEventListener("click", onclick, true);
        var selectedObject;
        var raycaster = new THREE.Raycaster();
       
    });
    

    webGLRenderer.domElement.addEventListener('dblclick', onDoubleClick, false)


    let controls = new PointerLockControls(camera, webGLRenderer.domElement);
    let clock = new THREE.Clock();

    
   
    
  /*  let btn1 = document.querySelector("#button1");
    btn1.addEventListener('click',()=>{
        controls.lock();
    });
*/

    let keyboard = [];
    addEventListener('keydown', (e)=>{
        keyboard[e.key] = true;
    });
    addEventListener('keyup', (e)=>{
        keyboard[e.key] = false;
    });
    window.addEventListener('resize', onWindowResize);
function onDoubleClick(event){
    const mouse = {
        x: (event.clientX / webGLRenderer.domElement.clientWidth) * 2 - 1,
        y: -(event.clientY / webGLRenderer.domElement.clientHeight) * 2 + 1,
    }
    raycaster.setFromCamera(mouse, camera)

    const intersects = raycaster.intersectObjects(scene.children);
   
     // Establezca el color de todos los modelos que se cruzan en rojo, si solo necesita activar el primer evento, luego cambie el color del primer modelo en la matriz
     for ( var i = 0; i < intersects.length; i++ ) {

        intersects[ i ].object.material.color.set( 0xff0000 );
        console.log(intersects[i].point);
        if(intersects[i].point.z == -30){
            console.log("is a cup of coffee");
            window.location.href = "http://localhost:/8000/auditorio.html";

        }

    }
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    webGLRenderer.setSize(window.innerWidth, window.innerHeight);
    drawScene();
}

    function processKeyboard() {
        let speed = 0.5;
        if(keyboard['w']){
            controls.moveForward(speed);
        }
        if(keyboard['s']){
            controls.moveForward(-speed);
        }
        if(keyboard['a']){
            controls.moveRight(-speed);
        }
        if(keyboard['d']){
            controls.moveRight(speed);
        }
    }

    function drawScene() {
        webGLRenderer.render(scene,camera);
        processKeyboard();
    // controls.lock();
     
        requestAnimationFrame(drawScene);
    }

    drawScene();



