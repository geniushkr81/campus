import * as THREE from '../build/three.module.js';

import { RoomEnvironment } from '../jsm/environments/RoomEnvironment.js';
import { OrbitControls } from '../jsm/controls/OrbitControls.js';
import { GLTFLoader } from '../jsm/loaders/GLTFLoader.js';

import { KTX2Loader } from '../jsm/loaders/KTX2Loader.js';
import { MeshoptDecoder } from '../jsm/libs/meshopt_decoder.module.js';


let camera, scene, renderer;
let geometry, material, mesh;
let textLoader;

const manager = new THREE.LoadingManager();
manager.onStart = function(url, itemsLoaded, itemsTotal){
    var progress = document.createElement('div');
document.body.appendChild(progress);
progress.innerHTML = "cargando....";
		/* 	const scene0 = new THREE.Scene();
			const camera0 = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

			const renderer0 = new THREE.WebGLRenderer();
			renderer0.setSize( window.innerWidth, window.innerHeight );
			document.body.appendChild( renderer0.domElement );

    var loader = new THREE.FontLoader();
      loader.load( 'https://raw.githubusercontent.com/mrdoob/three.js/master/examples/fonts/helvetiker_bold.typeface.json', function ( font ) {

     textLoader = new THREE.TextGeometry("Cargando Scene.....",{
        font: font,
        size: 60,
        height: 60,
        curveSegments: 20
      });
    });
    scene0.add(textLoader);
    renderer0.render(scene0,camera0); */
    console.log('Started loading file');   
     

};
manager.onLoad = function() {
    console.log('Loading complete!');
};
manager.onProgress = function(url, itemsLoaded, itemsTotal){
    console.log('loading file!');
    var progress = document.createElement('div');
    document.body.appendChild(progress);
    progress.innerHTML = "cargando....";
};
manager.onError = function(url){
    console.log('There was an error loading!');
};


init();
//render();

function init() {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
    camera.position.set(0, 100, 0);
    

    renderer = new THREE.WebGLRenderer( { antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1;
    renderer.outputEncoding = THREE.sRGBEncoding;


    const environment = new RoomEnvironment();
	const pmremGenerator = new THREE.PMREMGenerator( renderer );
    
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x000000);
    scene.fog = new THREE.Fog( 0xcce0ff, 500, 10000 );
    scene.environment = pmremGenerator.fromScene(environment).texture;


    // create box mesh 
  //  geometry = new THREE.BoxGeometry(0.2, 0.2, 0.2);
   // material = new THREE.MeshNormalMaterial();

   // mesh = new THREE.Mesh(geometry, material);
   // scene.add(mesh);

  //  renderer.setAnimationLoop(animation);
    document.body.appendChild(renderer.domElement);

   const ktx2Loader = new KTX2Loader()
            .setTranscoderPath('../js/libs/basis/')
            .detectSupport(renderer);

   const loader = new GLTFLoader(manager).setPath('../models/');
    loader.setKTX2Loader(ktx2Loader);
    loader.setMeshoptDecoder(MeshoptDecoder);
    loader.load('campus_01.glb', function(gltf) {
        gltf.scene.position.y = 8;
        scene.add(gltf.scene);
        render();
    });

    /*
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change', render); // use if there is no animation loop
    controls.minDistance = 100;
    controls.maxDistance = 2000;
    controls.target.set(10, 90, -16);
    // How far you can orbit vertically, upper and lower limits.
	// Range is 0 to Math.PI radians.
	controls.minPolarAngle = 0; // radians
	controls.maxPolarAngle = Math.PI; // radians
   
    controls.update();
*/
    window.addEventListener('resize', onWindowResize);
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
}
function render() {
    renderer.domElement.addEventListener('mousemove', event => {
        if(!this.press){ return }

        if(event.button == 0){
            camera.position.y -= event.movementY * this.sensitivity
            camera.position.x -= event.movementX * this.sensitivity        
        } else if(event.button == 2){
            camera.quaternion.y -= event.movementX * this.sensitivity/10
            camera.quaternion.x -= event.movementY * this.sensitivity/10
        }

   //     updateCallback();
    });
    renderer.render(scene, camera);
}

function animation(time){
    mesh.rotation.x = time / 2000;
    mesh.rotation.y = time / 1000;
    renderer.render(scene, camera);
}
