import * as THREE from '../build/three.module.js';

import { RoomEnvironment } from '../jsm/environments/RoomEnvironment.js';
import { OrbitControls } from '../jsm/controls/OrbitControls.js';
import { GLTFLoader } from '../jsm/loaders/GLTFLoader.js';

import { KTX2Loader } from '../jsm/loaders/KTX2Loader.js';
import { MeshoptDecoder } from '../jsm/libs/meshopt_decoder.module.js';
import { PointerLockControls } from '../jsm/controls/PointerLockControls.js';


    // create a scene, that will hold all our elements such as objects, cameras nad lights.
    var scene = new THREE.Scene();
    // create a camera, wich defines where we`re looking at.
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    // create a render and set the size
    var webGLRenderer = new THREE.WebGLRenderer({antialias: true});
  //  scene.background = new THREE.Texture("../images/sky.jpeg");
      // Load the background texture
     //Load background texture
const loaderTexture = new THREE.TextureLoader();
loaderTexture.load('./images/sky.jpeg' , function(texture)
            {
             scene.background = texture;  
            });
    // Color(0x000000);
    webGLRenderer.setSize(window.innerWidth, window.innerHeight);
    // position and point the camera to the center of the scene
    camera.position.x = -8;
    camera.position.y = 13;
    camera.position.z = 27;
    document.body.appendChild(webGLRenderer.domElement);
    
    var ambientLight = new THREE.AmbientLight(0x5e5d57,0);
    scene.add(ambientLight);
    // add spotlight for the shadows
    var spotLight = new THREE.SpotLight(0x0d0082);
    spotLight.position.set(50,50,50);
    spotLight.intensity = 1;
    scene.add(spotLight);
    var spotLight_1 = new THREE.SpotLight(0xd1b32a);
    spotLight_1.position.set(30,10,13);
    spotLight_1.intensity = 1;
    scene.add(spotLight_1);
    // add the output of the renerer to the html element
    let grid = new THREE.GridHelper(100, 20, 0x0a0a0a, 0x0a0a0a);
    grid.position.set(0, -0.5, 0);
    scene.add(grid);
/*
    let bGeo = new THREE.BoxGeometry(1,1,1);
    let bMat = new THREE.MeshStandardMaterial({color:0x00ff00, wireframe:false});
    let cube = new THREE.Mesh(bGeo, bMat);
    scene.add(cube);

*/
    const ktx2Loader = new KTX2Loader()
    .setTranscoderPath('../js/libs/basis/')
    .detectSupport(webGLRenderer);

    const loader = new GLTFLoader();
    loader.setKTX2Loader(ktx2Loader);
    loader.setMeshoptDecoder(MeshoptDecoder);
    loader.load('../models/auditorio.gltf', function(gltf) {
    gltf.scene.position.y = 0;
    gltf.scene.position.x = -18;
    gltf.scene.position.z = -15;
    scene.add(gltf.scene);
    drawScene();
    });
    

    let controls = new PointerLockControls(camera, webGLRenderer.domElement);
    let clock = new THREE.Clock();


    let btn1 = document.querySelector("#button1");
    btn1.addEventListener('click',()=>{
        controls.lock();
    });

    let keyboard = [];
    addEventListener('keydown', (e)=>{
        keyboard[e.key] = true;
    });
    addEventListener('keyup', (e)=>{
        keyboard[e.key] = false;
    });
    window.addEventListener('resize', onWindowResize);

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    webGLRenderer.setSize(window.innerWidth, window.innerHeight);
    drawScene();
}

    function processKeyboard() {
        let speed = 0.2;
        if(keyboard['w']){
            controls.moveForward(speed);
        }
        if(keyboard['s']){
            controls.moveForward(-speed);
        }
        if(keyboard['a']){
            controls.moveRight(-speed);
        }
        if(keyboard['d']){
            controls.moveRight(speed);
        }
    }

    function drawScene() {
        webGLRenderer.render(scene,camera);
        processKeyboard();
        requestAnimationFrame(drawScene);
       // controls.lock();
    }

    drawScene();



